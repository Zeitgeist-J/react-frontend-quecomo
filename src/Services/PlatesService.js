import axios from 'axios';
import * as restaurantController from './RestaurantService';
import * as ingredientController from './IngredientsService';
import * as foodTypeController from './FoodTypeService';
import * as commentController from './CommentsService';
import { endpoint } from '../assets/Constants';


export const getPlatos = async () => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/plate`).then((response) => {
            response = response.data.data;
            Promise.all(response
                .map(plate => restaurantController.getRestaurant(plate.restaurant_id))
            ).then(restaurants => {
                Promise.all(response
                    .map(plate => foodTypeController.getFoodType(plate.type_food_id))
                ).then(foodTypes => {
                    Promise.all(response
                        .map(plate => commentController.getComentariosByPlate(plate.id))
                    ).then(comments => {
                        Promise.all(response
                            .map(plate => ingredientController.getIngredientes(plate.ingredients))
                        ).then(igs => {
                            response.forEach((plate, i) => {
                                plate.restaurant = restaurants[i];
                                plate.foodType = foodTypes[i];
                                plate.comments = comments[i];
                                plate.ingredients = igs[i];
                            });
                            resolve(response);
                        })
                    })
                })
            }).catch(err => reject(err));
        }).catch(err => reject(err));
    } catch (err) {
        reject(err);
    }
});

export const getPlate = async (plateId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/plate/${plateId}`).then((plate) => {
            plate = plate.data.data;
            restaurantController.getRestaurant(plate.restaurant_id).then(restaurant => {
                plate.restaurant = restaurant;
                foodTypeController.getFoodType(plate.type_food_id).then(foodType => {
                    plate.foodType = foodType;
                    commentController.getComentariosByPlate(plate.id).then(comments => {
                        plate.comments = comments;
                        ingredientController.getIngredientes(plate.ingredients).then((ingredients) => {
                            plate.ingredients = ingredients;
                            resolve(plate);
                        });
                    });
                });
            });
        }).catch(err => reject(err));
    }
    catch (err) {
        reject(err)
    }

});