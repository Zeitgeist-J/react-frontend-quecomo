import axios from 'axios';
import { endpoint } from '../assets/Constants';

export const login = (userObject) => new Promise((resolve, reject) => {
    try {
        axios.post(`${endpoint}/login`, userObject).then(user => {
            delete user.data.data.password;
            localStorage.setItem('user', JSON.stringify(user.data.data));
            resolve();
        }).catch(err => reject(err));
    }
    catch (e) {
        reject(e)
    }
});

export const register = (registerObject) => new Promise((resolve, reject) => {
    try {
        axios.post(`${endpoint}/user`, registerObject).then((user) => {
            delete user.data.data.password;
            localStorage.setItem('user', JSON.stringify(user.data.data));
            resolve();
        }).catch(err => reject(err));
    }
    catch(e) {
        reject(e);
    }
});

export const getUser = (userId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/user/${userId}`).then(user => {
            delete user.data.data.password;
            resolve(user.data.data);
        }).catch(err => reject(err));
    } catch(e) {
        reject(e);
    }
});

export const getLoggedUser = () => new Promise((resolve, reject) => {
    try {
        const user = JSON.parse(localStorage.getItem('user'))
        resolve(user);
    }
    catch (err) {
        reject(err);
    }
}) 
