import axios from 'axios';
import { endpoint } from '../assets/Constants';

export const getIngredientes = async (ingredientsArray) => new Promise((resolve, reject) => {
    try {
        if (!ingredientsArray?.length) {
            console.log('no ingredients');
            resolve([]);
        }
        Promise.all(ingredientsArray.map(id => getOneIngredient(id))).then(response => {
            resolve(response)
        }).catch(err => {
            reject(err)
        });
    }
    catch (e) {
        reject(e);
    }
});


export const getOneIngredient = async (ingredientId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/ingredient/${ingredientId}`).then((ingredient) => {
            resolve(ingredient.data.data);
        }).catch((err) => reject(err));
    }
    catch (e) {
        reject(e)
    }
});