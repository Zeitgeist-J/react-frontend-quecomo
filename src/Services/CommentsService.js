import axios from 'axios';
import { endpoint } from '../assets/Constants';
import * as userController from './UserService';


export const getComentariosByPlate = (plateid) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/commentary/${plateid}/plate`).then((comments) => {
            if (!comments.data.data.length) {
                resolve([]);
            }
            Promise.all(comments.data.data.map(comment => userController.getUser(comment.user_id)))
                .then(users => {
                    comments.data.data.forEach((comment, index) => {
                        comment.user = users[index];
                    });
                    resolve(comments.data.data);
                });
        }).catch(err => reject(err));
    } catch (err) {
        reject(err);
    }
});


export const getComentariosByRestaurant = (restaurantId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/commentary/${restaurantId}/restaurant`).then((response) => {
            resolve(response.data.data);
        }).catch(err => reject(err));
    } catch (err) {
        reject(err);
    }
});

export const postComment = (comment) => new Promise((resolve, reject) => {
    try {
        axios.post(`${endpoint}/commentary`, comment).then(response => {
            resolve(response.data.data);
        }).catch(err => {
            reject(err);
        });
    }
    catch (e) {
        reject(e);
    }
});

export const commentLike = (comment) => new Promise((resolve, reject) => {
    try {
        axios.put(`${endpoint}/commentary/like`, comment).then(response => {
            resolve(response.data.data);
        }).catch(err => {
            reject(err);
        });
    }
    catch (e) {
        reject(e)
    }
});
