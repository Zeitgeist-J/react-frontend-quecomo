import axios  from 'axios';
import { endpoint } from '../assets/Constants';

export const getFoodType = async(typeId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/typefood/${typeId}`).then(foodType => {
            resolve(foodType.data.data);
        }).catch(err => reject(err));
    } catch (e) {
        reject(e)
    }
});