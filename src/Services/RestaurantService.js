import axios from 'axios';
import { endpoint } from '../assets/Constants';

export const getRestaurant = async (restaurantId) => new Promise((resolve, reject) => {
    try {
        axios.get(`${endpoint}/restaurant/${restaurantId}`).then(restaurant => {
            resolve(restaurant.data.data);
        }).catch(err => reject(err));
    }
    catch (e) {
        reject(e)
    }
});