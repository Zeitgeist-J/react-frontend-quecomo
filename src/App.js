import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

const HomeModule = lazy(() => import('./modules/HomeModule'));
const UserModule = lazy(() => import('./modules/UserModule'));
const SignUpModule = lazy(() => import('./modules/SignUpModule'));
const Page404 = lazy(() => import('./pages/NotFound'));

const createStyles = makeStyles({
  screen: {
    display: 'flex',
    flex: 1,
    height: '100vh',
    width: '100vw',
    justifyContent: 'center',
    justifyItems: 'center'
  }
});

function App() {
  const styles = createStyles();
  return (
    <Grid container className={styles.screen}>
      <Router >
        <Suspense fallback={<div><h1>Loading...</h1></div>}>
          <Switch>
            <Redirect exact from="/" to="/home" />
            <Route path="/home" component={HomeModule} />
            <Route path="/user" component={UserModule} />
            <Route path="/signup" component={SignUpModule} />
            <Route path="*" component={Page404} />
          </Switch>
        </Suspense>
      </Router>
    </Grid>
  );
}


export default App;
