import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles'
import { Rating } from '@material-ui/lab';
import { Box } from '@material-ui/core'

const createStyles = makeStyles({
    container: {
        display: 'flex',
        alignItems: 'stretch',
        alignSelf: 'center',
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 0
    }
});

const Calification = (props) => {
    const [value, setValue] = useState(0);

    const styles = createStyles();

    useEffect(() => {
        setValue(props.valueReceive);
    }, [props.valueReceive])

    const calificationHandler = (event, newValue) => {
        setValue(newValue);
        props.value(newValue)
    }

    return <Box className={styles.container} component="fieldset" mb={3} borderColor="transparent">
        <h3>Calificación</h3>
        {props.readOnly ?
            <Rating
                name="read-only"
                readOnly
                value={value}
            /> :
            <Rating
                name="simple-controlled"
                value={value}
                onChange={calificationHandler}
            />
        }
    </Box>
}
export default Calification;