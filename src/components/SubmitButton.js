import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Button,
} from '@material-ui/core';
import { theme } from '../assets/Constants';

const createStyles = makeStyles({
    Button: {
        color: 'white',
        backgroundColor: theme.buttonColor,
        marginBottom: 20
    }
});

const SubmitButton = props => {
    const styles = createStyles();

    return (
        <Button
            {...props}
            className={`${props.className} ${styles.Button}`}
            variant="contained">
                {props.children}
        </Button>
    )
}

export default SubmitButton;