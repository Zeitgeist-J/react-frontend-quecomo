import React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';


const createStyles = makeStyles({
    screen: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100vw',
        minHeight: '100vh'
    },
    body: {
        fontSize: 34,
        color: 'red'
    }
});

const Charger = () => {
    const styles = createStyles();

    return (
        <Grid container className={styles.screen}>
            <p className={styles.body}>...Loading</p >
        </Grid >
    );
}

export default Charger;