import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FoodCard from './FoodCard';
import * as plateController from '../Services/PlatesService';


const createStyles = makeStyles({
  screen: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 30,
    position: 'absolute'
  },
  cardContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginBottom: 35
  }
});


const FoodList = props => {
  const [plates, setPlates] = useState([]);
  const [foodList, setFoodList] = useState([]);


  const styles = createStyles();

  useEffect(() => {
    const getPlates = async () => {
      try {
        plateController.getPlatos().then(plt => {
          //console.log(plates)
          setPlates(plt);
          setFoodList(plt);
        });
      } catch (err) {
        console.log(err + " unable to reach server")
      }
    }

    getPlates();
  }, []);


  useEffect(() => {
    if (props.filter) {
      const list = plates.filter(plate => (plate.foodType.type_food_name.toLowerCase().includes(props.filter.toLowerCase())
        || plate.restaurant.restaurant_name.toLowerCase().includes(props.filter.toLowerCase())
        || plate.ingredients.find(ig => ig.ingredient_name.toLowerCase().includes(props.filter.toLowerCase())))
        || plate.plate_name.toLowerCase().includes(props.filter.toLowerCase())
      );
      setFoodList(list)
    } else {
      setFoodList(plates)
    }
  }, [props.filter, plates]);


  return (
    <Grid container>
      {(!foodList.length || !plates.length) ?
        <Grid container className={styles.screen}>
          <h1>Sin resultados</h1>
        </Grid> :

        <Grid container className={styles.screen}>
          <Grid className={styles.cardContainer} container>
            <Grid container item xs={8} spacing={3}>
              {
                foodList.map((plate, index) => {
                  return <FoodCard key={index} plate={plate} />


                })
              }
            </Grid>
          </Grid>

          {/* <Grids /> */}
        </Grid >

      }

    </Grid>

  )

}

export default FoodList;
