import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Grid } from '@material-ui/core';
import Card from './Card';
import SubmitButton from './SubmitButton';
import Calification from './Calification';
import * as userController from '../Services/UserService';
import * as commentController from '../Services/CommentsService';

const createStyles = makeStyles({
    textField: {
        backgroundColor: 'white',
        alignSelf: 'start',
        width: '100%',
        borderRadius: 10
    },
    buttonContainer: {
        display: 'flex',
        alignItems: 'stretch'
    },
    button: {
        height: '100%',
        width: '100%'
    },
    error: {
        fontSize: 14,
        color: 'red',
        marginTop: 0,
        marginBottom: 0
    }
});



const CommentCreator = props => {
    const [comment, setComment] = useState('');
    const [score, setScore] = useState(0);
    const [user, setUser] = useState(null);
    const [error, setError] = useState(null);

    const styles = createStyles();

    useEffect(() => {
        userController.getLoggedUser().then(usr => {
            setUser(usr)
        }).catch(err => setUser(null));
    }, [])

    const commentHandler = commentText => {
        setComment(commentText.target.value);
    }

    const scoreHanler = score => {
        setScore(score);
    }

    const createComment = () => {
        userController.getLoggedUser().then(usr => {

            if (!user) {
                setError(<h6 className={styles.error}>
                    Debes iniciar sesión para poder comentar
                </h6>
                );
                return;
            }
            const commentObject = {
                restaurant_id: props.restaurant,
                plate_id: props.plate,
                user_id: user.id,
                commentary: comment,
                score: score,
                publication_date: new Date(),
                likes: 0,
            };

            commentController.postComment(commentObject).then(resp => {
                setComment('');
                setScore(0);
                props.comment(resp);
            }).catch(err => {
                return;
            });
        }).catch(err => {
            setError(<h6 className={styles.error}>
                Error inesperado
            </h6>);
        });

    }


    return (
        <Card>
            <Grid item xs={9}>
                <TextField
                    className={styles.textField}
                    value={comment}
                    id="outlined-multiline-static"
                    label="Ingresa tu comentario"
                    multiline
                    rows={3}
                    variant="outlined"
                    onChange={commentHandler}
                />
            </Grid>
            <Calification value={scoreHanler} valueReceive={score} />
            <Grid item xs={2} className={styles.buttonContainer}>
                <SubmitButton
                    disabled={(!comment || !score)}
                    className={styles.button}
                    onClick={createComment}>
                    Guardar
                </SubmitButton>
            </Grid>
            {error}
        </Card>
    );
}

export default CommentCreator;