import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FavoriteRounded } from '@material-ui/icons';
import { theme } from '../assets/Constants';
import Card from './Card';
import Calification from './Calification';
import * as commentController from '../Services/CommentsService';


const createStyles = makeStyles({
    cardContainer: {
        display: 'flex',
        background: theme.accentColor,
        borderRadius: 10,
        padding: 10,
        margin: 10,
        justifyContent: 'space-around'
    },
    image: {
        width: 70,
        height: 70,
        margin: 10,
        borderRadius: 50,
        alignSelf: 'center'
    },
    noMargin: {
        margin: 5
    },
    small: {
        fontSize: 12,
        marginLeft: 5
    },
    textAlignment: {
        textAlign: 'center',
        margin: 5
    },
    dataContainer: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    }
});


const CommentViewer = props => {
    const styles = createStyles();
    // console.log(props.comment)
    //

    const commentLike = () => {
        props.comment.likes += 1;
        console.log(props.comment)
        commentController.commentLike(props.comment).then(response =>{
            props.likeUpdate();
        })
    }

    return (
        <Card>
            <Grid item container xs={6}>
                <img
                    className={styles.image}
                    src="https://quecomoproyect.s3-us-west-1.amazonaws.com/anonymous-user.png"
                    alt="user" />
                <Grid item className={styles.dataContainer}>
                    <h4 className={styles.noMargin}>
                        {props.comment.user?.email ?? 'Anonymous'}
                    </h4>
                    <small className={styles.small}>
                        {props.comment.publication_date ?? new Date()}
                    </small>
                    <p className={styles.noMargin}>
                        {props.comment.commentary ?? '...cargando comentario'}
                    </p>
                </Grid>
            </Grid>
            <Grid item className={styles.dataContainer} xs={4}>
                <Calification readOnly={true} valueReceive={props.comment.score ?? 0}/>
            </Grid>
            <Grid item
                className={styles.dataContainer}>
                <Button onClick={commentLike}>
                    <FavoriteRounded />
                    <p className={styles.textAlignment}>
                        {props.comment.likes ?? 0}
                    </p>
                </Button>
            </Grid>
        </Card>
    )
}

export default CommentViewer;