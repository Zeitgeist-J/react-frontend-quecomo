import React, { useState } from 'react';
import { Grid, TextField, InputAdornment } from '@material-ui/core';
import { SearchRounded } from '@material-ui/icons'


import { makeStyles, styled } from '@material-ui/core/styles';


const createStyles = makeStyles({
    BarContainer: {
        marginTop: 30,
    },
    littleBar: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

const Input = styled(TextField)({
    borderRadius: 10,
    width: '100%'
});

const SearchBar = props => {
    const [value, setValue] = useState('')
    const styles = createStyles();

    const valueHandler = text => {
        setValue(text.target.value);
        props.onChangeText(text.target.value);
    }

    return (
        <Grid container
            className={styles.BarContainer}
            direction="row"
            justify="center"
            alignItems="center">
            <Grid item
                className={styles.littleBar}
                xs={12}
                sm={8}
                md={6}
                lg={4}
                xl={2}>
                <Input
                    className={styles.margin}
                    id="input-with-icon-textfield"
                    label="Restaurante o comida"
                    placeholder="Busca el restaurante o comida específica que desees"
                    variant="filled"
                    value={value}
                    onChange={valueHandler}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <SearchRounded />
                            </InputAdornment>
                        ),
                    }}
                />
            </Grid>
        </Grid>

    );
}

export default SearchBar;