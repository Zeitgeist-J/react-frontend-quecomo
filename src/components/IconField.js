import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Logo from '../assets/images/Logo.png';
import LoginIcon from '../assets/images/LoginIcon.png';
import SignUpIcon from '../assets/images/SignUpIcon.png';

const createStyles = makeStyles({
    screen: {
        display: 'flex',
        justifyContent: 'center'
    },
    Icons: {
        marginTop: 20
    },
    IconsSection: {
        marginTop: 30
    }
})


const IconField = () => {
    const styles = createStyles();

    return (
        <Grid container className={styles.screen}>
            <Grid item xs={10}>
                <Grid container justify="center" className={styles.Icons} spacing={8}>
                    <Grid item>
                        <a href="/">
                            <img
                                className='Logo'
                                src={Logo}
                                alt=" Logo"
                                title="QueComoLogo"
                                height="120" />
                        </a>
                    </Grid>
                    <Grid item className={styles.IconsSection}>
                        <div className='LoginContainer'>
                            <a className='LoginButton' href="/user">
                                <img src={LoginIcon} alt="Login" title="Login" width="30" height="30" ></img>
                            </a>
                        </div>
                        <div className='SignUpContainer'>
                            <a className='SignUpButton' href="/signup">
                                <img src={SignUpIcon} alt="SignUp" title="SignUp" width="30" height="30" ></img>
                            </a>
                        </div>
                    </Grid>

                </Grid>
            </Grid>
        </Grid>
    );
}


export default IconField;
