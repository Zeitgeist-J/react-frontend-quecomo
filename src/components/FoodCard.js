import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import '../assets/css/FoodCard.css';
import ScoreIcon from '../assets/images/scoreIcon.png';
import CommentsIcon from '../assets/images/commentsIcon.png';
import { Link } from 'react-router-dom';
import * as commentsController from '../Services/CommentsService';

const createStyles = makeStyles({
    // container: props => ({
    //     background: `url(${props.image})`,
    // }),
    container: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#D3DFE5',
        borderRadius: 20,
        marginBottom: 7,
        marginTop: 7,
        justifyContent: 'space-around',
        '&:hover': {
            background: "#c7d2d8",
        },
    },
    imageContainer: {
        width: 170,
        height: 170,
        marginBottom: 15,
        marginTop: 15,
        marginLeft: 30,
        marginRight: 30,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 30,
    },
    link: {
        width: '100%'
    }
});


const FoodCard = props => {
    const styles = createStyles(props);
    const plate = props.plate;
    const [comments, setComments] = useState([]);

    useEffect(() => {
        const getComments = async () => {
            try {
                commentsController.getComentariosByPlate(plate.id).then(comments => {
                    setComments(comments);
                });
            } catch (err) {
                console.log(err + " unable to reach sever")
            }
        }
        getComments();
    }, [plate]);

    return (
        <Link className={styles.link} to={`home/plate/${plate.id}`} >
            <Grid container className={styles.container}>
                <Grid item>
                    <img src={plate.plate_picture}
                        className={styles.imageContainer}
                        alt="food_image" height="250" />
                </Grid>
                <Grid item><br />
                    <h2>{plate.plate_name}</h2>
                    <p id="restaurante">{plate.restaurant?.restaurant_name}</p>
                    <table>
                        <tbody>
                            <tr>
                                <th id="row"><p>Puntuación:</p>
                                    <img src={ScoreIcon}
                                        id="icon" alt="Score"
                                        title="Score" width="15"
                                        height="15" />
                                    <p id="icon">{plate.plate_score}</p>
                                </th>
                                <th><p>Comentarios:</p>
                                    <img src={CommentsIcon}
                                        id="icon" alt="Score"
                                        title="Score" width="15"
                                        height="15" />
                                    <p id="icon">{comments?.length ?? 0}</p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </Grid>
                <Grid item>
                    <h4>Contiente:</h4>
                    {plate.ingredients.map(ingredient => <p key={ingredient.id}>{ingredient.ingredient_name}</p>)}
                </Grid>

            </Grid>
        </Link>

    );
}

export default FoodCard;
