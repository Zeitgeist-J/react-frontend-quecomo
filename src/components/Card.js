import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { theme } from '../assets/Constants';

const createStyles = makeStyles({
    cardContainer: {
        display: 'flex',
        background: theme.accentColor,
        borderRadius: 10,
        padding: 10,
        margin: 10,
        justifyContent: 'space-around'
    }
});

const Card = props => {
    const styles = createStyles();
    return (
        <Grid container className={`${styles.cardContainer} ${props.className}`}>
            {props.children}
        </Grid>
    )
};

export default Card;