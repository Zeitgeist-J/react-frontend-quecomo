import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';


const Login = lazy(() => import('../pages/Login'));
const Page404 = lazy(() => import('../pages/NotFound'));
const Register = lazy(() => import('../pages/Register'));

const createStyle = makeStyles({
    screen: {
        flex: 1,
        width: '100vw',
        height: '100vh'
    }
});


const SignUpModule = () => {
    const styles = createStyle();

    let { path } = useRouteMatch();

    return (
        <div className={styles.screen}>
            <Router>
                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route exact path={`${path}/`} component={Register} />
                        <Route path={`${path}/user`} component={Login} />
                        <Route path="*" component={Page404} />
                    </Switch>
                </Suspense>
            </Router>
        </div>
    );
}


export default SignUpModule;
