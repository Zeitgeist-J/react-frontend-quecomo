import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Charger from '../components/Charger';

const Home = lazy(() => import('../pages/Home'));
const Page404 = lazy(() => import('../pages/NotFound'));
const PlateViewer = lazy(() => import('../pages/PlateViewer'));

const createStyle = makeStyles({
    screen: {
        flex: 1,
        width: '100vw',
        height: '100vh',
    }
});


export default () => {
    const styles = createStyle();

    let { path } = useRouteMatch();

    return (
        <div className={styles.screen}>
            <Router>
                <Suspense fallback={<Charger />}>
                    <Switch>
                        <Route exact path={`${path}/`} component={Home} />
                        <Route path={`${path}/plate/:id`} component={PlateViewer} />
                        <Route path="*" component={Page404} />
                    </Switch>
                </Suspense>
            </Router>
        </div>

    )
};