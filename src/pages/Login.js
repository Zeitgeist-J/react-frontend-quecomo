import React, { useState, useEffect, lazy } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Logo from '../assets/images/Logo.png';
import SubmitButton from '../components/SubmitButton';
import * as userController from '../Services/UserService';
import {
    TextField,
    Grid
} from '@material-ui/core';
import {
    BrowserRouter,
    Redirect,
    Route,
} from 'react-router-dom';

const App = lazy(() => import('../modules/HomeModule'))

const loginStyles = makeStyles({
    screen: {
        flex: 1,
        height: '100vh',
        width: '100vw',
        alignItems: 'center',
    },
    dataContainer: {
        flex: 'column',
        justifyItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#D3DFE5',
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 10
    },
    text: {
        textAlign: 'center'
    },
    imputField: {
        backgroundColor: 'white',
        marginBottom: 7,
        borderRadius: 5
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 30,
        marginBottom: 10
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: 30,
        justifyItems: 'center',
        justifyContent: 'center'
    },
    errorText: {
        textAlign: 'center',
        color: 'red',
    },
    link: {
        textAlign: 'center',
        marginTop: 5,
        marginBottom: 5,
    }
});

const Login = () => {
    const [email, setEmail] = useState('');
    const [passwd, setPasswd] = useState('');
    const [validForm, setValidForm] = useState('');
    const [errorMessages, setErrorMessages] = useState('');
    const [redirect, setRedirect] = useState('');


    const styles = loginStyles();

    const emailChanges = props => {
        setEmail(props.target.value)
    }

    const passwordChanges = props => {
        setPasswd(props.target.value)
    }

    const loginUsuarios = () => {
        const loginData = {
            'email': email,
            'password': passwd,
        };
        userController.login(loginData).then(() => {
            delete loginData.password;
            setRedirect(<BrowserRouter forceRefresh={true}>
                <Redirect to="/" />
                <Route path="/" component={App} />
            </BrowserRouter>,
                document.getElementById('root')
            );
        }).catch((err) => {
            setErrorMessages(<h6 className={styles.errorText}>Correo o contaseña incorrecta</h6>);
        });
    }

    useEffect(() => {
        if (email && passwd) {
            setValidForm(true);
            return;
        } else {
            setValidForm(false);
        }
    }, [email, passwd])

    return (
        <Grid className={styles.screen} container justify='center'>
            <Grid item>
                <img src={Logo} height="150" alt="logo"></img>
                <h3 className={styles.text}>Ingresa a tu cuenta</h3>
                {errorMessages}
                <Grid container className={styles.dataContainer}>
                    <Grid xs={10} item>
                        <form className={styles.form}>
                            <TextField className={styles.imputField}
                                value={email}
                                onChange={emailChanges}
                                id="Email"
                                label="Email"
                                type="email"
                                variant="outlined" />
                            <TextField className={styles.imputField}
                                value={passwd}
                                onChange={passwordChanges}
                                id="password"
                                type="password"
                                label="Contraseña"
                                variant="outlined" />
                        </form>
                    </Grid>
                    <Grid item xs={10} className={styles.buttonContainer}>
                        <SubmitButton
                            onClick={loginUsuarios}
                            disabled={!validForm}>
                            Ingresar
                        </SubmitButton>
                    </Grid>
                </Grid>
                <a href="/home">
                    <p className={styles.link}>Volver a la página principal.</p>
                </a>
                {redirect}
            </Grid>
        </Grid>
    );
}


export default Login;
