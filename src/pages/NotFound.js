import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Logo from '../assets/images/Logo.png';


const createStyles = makeStyles({
    screen: {
        height: '100vh',
        justifyItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        fontWeight: 'bold',
        backgroundColor: '#FFE9AD'
    },
    contentContainer: {
        flexDirection: 'column',
        justifyItems: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlign: 'center'
    },
    image: {
        justifySelf: 'center',
        borderWidth: 3,
        borderColor: '#E8A89E',
        borderRadius: 10
    }

});

const NotFound = () => {
    const styles = createStyles();

    return (
        <Grid container className={styles.screen}>
            <Grid item className={styles.image}>
                <img className={styles.image} src={Logo} alt='logo' width="200" ></img>
            </Grid>
            <Grid item xs={12} className={styles.contentContainer}>
                <h1 className={styles.text}>Página no encontrada</h1>
                <p className={styles.text}>Intenta otra ruta</p>
            </Grid>
            <a href="/home">
                <Button variant="contained" color="primary">
                    Home
                </Button>
            </a>

        </Grid>
    );
}



export default NotFound;

