import React, {
    useState,
    useEffect,
    lazy
} from 'react';
import {
    BrowserRouter,
    Redirect,
    Route,
} from 'react-router-dom';
import Logo from '../assets/images/Logo.png';
import { makeStyles } from '@material-ui/core/styles';
import {
    TextField,
    Grid
} from '@material-ui/core';
import * as userController from '../Services/UserService';
import SubmitButton from '../components/SubmitButton';



const createStyles = makeStyles({
    screen: {
        flex: 1,
        height: '100vh',
        alignItems: 'center'
    },
    dataContainer: {
        flex: 'column',
        justifyItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#D3DFE5',
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 10
    },
    text: {
        textAlign: 'center'
    },
    imputField: {
        backgroundColor: 'white',
        marginBottom: 7,
        borderRadius: 5
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 30,
        marginBottom: 10
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: 30,
        justifyItems: 'center',
        justifyContent: 'center'
    },
    link: {
        textAlign: 'center',
        marginTop: 5,
        marginBottom: 5,
    }, error: {
        color: 'red'
    }
});

const App = lazy(() => import('../modules/HomeModule'))

const Register = () => {
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [passwd, setPasswd] = useState('');
    const [passwdRpt, setPasswdRpt] = useState('');
    const [validForm, setValidForm] = useState(false);
    const [errorMessages, setErrorMessages] = useState([]);
    const [redirect, setRedirect] = useState('');

    // let redirect = '';

    const styles = createStyles();
    const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i


    useEffect(() => {
        if ((passwdRpt === passwd) && (passwd && passwdRpt && username && email) && (emailRegex.test(email))) {
            setValidForm(true);
            return;
        } else {
            setValidForm(false);
        }
    }, [email, emailRegex, passwd, passwdRpt, username])


    const registrarUsuario = () => {
        const registerData = {
            'role': 2,
            'user_name': username,
            'password': passwd,
            'email': email
        }
        userController.register(registerData).then(() => {
            delete registerData.password;
            setRedirect(<BrowserRouter forceRefresh={true}>
                <Redirect to="/" />
                <Route path="/" component={App} />
            </BrowserRouter>,
                document.getElementById('root')
            );
        }).catch((err) => {
            setErrorMessages([...errorMessages, <h5>{err}</h5>]);
        });
    }

    const emailHandler = props => {
        setEmail(props.target.value);
    }

    const usernameHandler = props => {
        setUsername(props.target.value);
    }

    const passwdHandler = props => {
        setPasswd(props.target.value);
    }

    const passwdRptHandler = props => {
        setPasswdRpt(props.target.value);
    }



    return (
        <Grid className={styles.screen} container justify="center">
            <Grid item >
                {/* <Link to="/"> */}
                <a href="/">
                    <img src={Logo} height="150" alt="logo"></img>
                </a>

                {/* </Link> */}
                <h3 className={styles.text}>Crea una cuenta</h3>
                <Grid container className={styles.dataContainer}>
                    <Grid xs={10} item>
                        <form className={styles.form}>
                            <TextField className={styles.imputField}
                                value={email}
                                onChange={emailHandler}
                                id="Email"
                                label="Email"
                                type="email"
                                variant="outlined" />
                            <TextField className={styles.imputField}
                                value={username}
                                onChange={usernameHandler}
                                id="Nombre de usuario"
                                label="Nombre de usuario"
                                type="text"
                                variant="outlined" />
                            <TextField className={styles.imputField}
                                value={passwd}
                                onChange={passwdHandler}
                                id="password"
                                type="password"
                                label="Contraseña"
                                variant="outlined" />
                            <TextField className={styles.imputField}
                                value={passwdRpt}
                                onChange={passwdRptHandler}
                                id="passwordRpt"
                                type="password"
                                label="Confirmar contraseña"
                                variant="outlined" />
                        </form>
                    </Grid>
                    <Grid item xs={10} className={styles.buttonContainer}>
                        <SubmitButton
                            onClick={registrarUsuario}
                            disabled={!validForm}>
                            Registrarse
                        </SubmitButton>
                        <h5 className={styles.link}>¿Ya tienes una cuenta?</h5>
                        <a href="/user">
                            <p className={styles.link}>inicia sesión acá.</p>
                        </a>
                    </Grid>
                </Grid>
                <a href="/home">
                    <p className={styles.link}>Volver a la página principal.</p>
                </a>
                {redirect}
            </Grid>
        </Grid>
    );
}

export default Register;
