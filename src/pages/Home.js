import React, { useState } from 'react';
import SearchBar from '../components/SearchBar';
import IconField from '../components/IconField';
import FoodList from '../components/FoodList';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';



const createStyle = makeStyles({
    screen: {
        display: 'flex',
        // flex: 1,
        minHeight: '100vh',
        width: '100vw',
        flexDirection: 'column',
        alignContent: 'space-between'
    }
});

const Home = props => {
    const [filter, setFilter] = useState(null);

    const styles = createStyle();

    const returnedValueHandler = filterText => {
        setFilter(filterText);
    }
    return (
        <Grid container className={styles.screen} >

            <IconField />
            <SearchBar onChangeText={returnedValueHandler} />
            <FoodList filter={filter} />
        </Grid>
    );
}


export default Home;