import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { theme } from '../assets/Constants';
import * as plateController from '../Services/PlatesService';
import Charger from '../components/Charger';
import Header from '../components/IconField';
import CommentViewer from '../components/CommentViewer';
import {
    useParams
} from "react-router-dom";
import CommentCreator from '../components/CommentCreator';



const createStyles = makeStyles({
    screen: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'start',
        alignItems: 'center',
        alignSelf: 'center',
        width: '100vw',
        minHeight: '100vh',
        scrollBehavior: 'auto',
        margin: 0
    },
    image: {
        borderRadius: 30,
        marginTop: 15,
        marginBottom: 15
    },
    imageContainer: {
        marginTop: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50,
        borderRadius: 10,
        background: theme.primaryColor
    },
    dataContainer: {
        marginLeft: 50,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    noMargin: {
        marginTop: 5,
        marginBottom: 5
    },
    commentContainers: {
        justifySelf: 'center',
        width: '90vw'
    }
});

const PlateViewer = () => {
    const [plate, setPlate] = useState(null);


    let { id } = useParams();
    const styles = createStyles();

    useEffect(() => {
        getPlate();
    }, []);

    const getPlate = async () => {
        try {
            plateController.getPlate(id).then((plt) => {
                setPlate(plt)
                console.log(plt);
            }).catch(err => {
                return;
            });
        } catch (err) {
            console.log(err + " unable to reach server")
        }
    }

    return plate ? (
        <Grid container className={styles.screen} >
            <Header className={`${styles.noMargin} ${styles.commentContainers}`} />
            <Grid item className={styles.imageContainer}>
                <img src={plate.plate_picture} className={styles.image} height="500" alt="plateImage" />
                <Grid item className={styles.dataContainer}>
                    <h1 className={styles.noMargin}>{plate.plate_name}</h1>
                    <h3 className={styles.noMargin}>{plate.foodType.type_food_name}</h3>
                    <h5 className={styles.noMargin}>{plate.restaurant.restaurant_name}</h5>
                    <p>Este plato contiene:</p>
                    <ol>
                        {plate.ingredients.map((ig, index) => {
                            return (<li key={index}>{ig?.ingredient_name}</li>);
                        })}
                    </ol>
                </Grid>
            </Grid>
            <Grid container className={`${styles.noMargin} ${styles.commentContainers}`} justify='center'>
                <Grid item xs={10} sm={8} md={6} lg={6} xl={6}>
                    <CommentCreator
                        plate={plate.id}
                        restaurant={plate.restaurant?.id}
                        comment={getPlate} />
                </Grid>
            </Grid>
            <Grid container className={`${styles.noMargin} ${styles.commentContainers}`} justify='center'>
                <Grid item xs={10} sm={8} md={6} lg={6} xl={6}>
                    {plate.comments.sort((a, b) => b.likes - a.likes).map((comment, index) =>
                        <CommentViewer key={index} comment={comment} likeUpdate={getPlate}/>
                    )}
                </Grid>
            </Grid>
        </Grid >) :
        <Charger />
        ;
}

export default PlateViewer;